/* Data structures */
String lines[];
int maxX = 0;
int maxY = 0;
int dim;
int[] select;
Point[] points;
Cluster[] clusters;

// K-means: k is the number of clusters
int k = 50;
boolean isFirst = true;
float prev_d = -1;
float prev_var = -1;
float curr_d;
float variation;

/* Round time
int roundTime = 3;
int meanFR = 26;
int globalC = 0;
*/

void setup() {
  size(700, 700);
  textAlign(RIGHT);
  textSize(20);
  surface.setTitle("K-means (k = " + k + ")");

  lines = loadStrings("a3.txt");
  dim = lines.length;

  points = new Point[dim];
  for(int i = 0; i < dim; i++) {
    String line[] = lines[i].split(" +");
    points[i] = new Point(int(line[1]), int(line[2]));

    // Keep max for scaling coordinates upon window size
    if(points[i].x > maxX)
      maxX = points[i].x;
    if(points[i].y > maxY)
      maxY = points[i].y;
  }

  // instantiate clusters
  clusters = new Cluster[k];
  int q;
  for(int i = 0; i < k; i++) {
    //clusters[i] = new Cluster();
    clusters[i] = new Cluster(points);
  }
}

void draw() {
  background(40);

  // draw actual centroids
  for(int i = 0; i < k; i++) {
    noStroke();
    fill(clusters[i].c);
    ellipse(clusters[i].center.x * width / maxX, clusters[i].center.y * height / maxY, 10, 10);
  }

  // each cluster with a different color
  for(int i = 0; i < points.length; i++) {
    //if has already a cluster
    if(points[i].cluster != -1) {
      stroke(clusters[points[i].cluster].c);
      point(points[i].x * width / maxX, points[i].y * height / maxY);
    } else {
      stroke(255);
      point(points[i].x * width / maxX, points[i].y * height / maxY);
    }
  }

  drawInfo();
  noLoop();
}

void keyPressed() {
  if(key == ENTER)
    if(!isFirst) {
      curr_d = 0;
      for(Cluster curr : clusters) {
        Point p = curr.evaluateCentroid();
        curr_d += dist(p.x, p.y, curr.center.x, curr.center.y);
        curr.center = p;
      }

      if(prev_d == -1) {
        prev_d = curr_d;
      } else {
        // TODO: Print variation and implement threshold
        variation = ((prev_d - curr_d) / prev_d);
        prev_d = curr_d;
      }

    }

    isFirst = false;
    oneStep();
}

void oneStep() {

  for(int i = 0; i < points.length; i++) {
    float mindist = MAX_FLOAT;
    float dist = -1;
    int cl = -1;
    Point c;

    for(int j = 0; j < clusters.length; j++) {
      c = clusters[j].center;
      dist = dist(points[i].x, points[i].y, c.x, c.y);
      if(dist < mindist) {
        mindist = dist;
        cl = j;
      }
    }

    points[i].distance = mindist;
    points[i].cluster = cl;
    clusters[cl].addMember(points[i]);
  }

  redraw();
}

void drawInfo() {
  if(!isFirst) {
    fill(255);
    variation *= 100;
    String tmp = nf(variation, 2, 2);
    text("Mean variation: " + tmp + " %\n", 0.99 * width, 0.04 * height);
  }
}

/*
 *
 *
 * Classes used
 *
 *
 */

class Point {
  int x;
  int y;
  int cluster; //goes from 0 to k-1
  float distance;
  boolean isCentroid;

  Point(int x, int y) {
    this.x = x;
    this.y = y;
    distance = -1;
    cluster = -1;
    isCentroid = false;
  }
}

class Cluster {
  ArrayList<Point> members;
  Point center;
  color c;

  // Initial centroid not belonging to dataset
  Cluster() {
    members = new ArrayList<Point>();
    c = color(random(255), random(255), random(255));
    center = new Point(int(random(maxX)), int(random(maxY)));
  }

  // Initial centroid belonging to dataset
  Cluster(Point[] points) {
    int q;
    members = new ArrayList<Point>();
    c = color(random(255), random(255), random(255));
    do {
      q = int(random(points.length - 1));
    } while (points[q].isCentroid);
    center = points[q];
    points[q].isCentroid = true;
  }

  void addMember(Point p) {
    members.add(p);
  }

  void removeMember(int index) {
    members.remove(index);
  }

  Point evaluateCentroid() {
    int xT = 0;
    int yT = 0;
    for(Point p : members) {
      xT += p.x;
      yT += p.y;
    }

    return new Point(int(xT / members.size()), int(yT / members.size()));
  }

}
